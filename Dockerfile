FROM alpine:latest

RUN rm -rf /etc/puppet
ADD puppet-config /etc/puppet
RUN chown -R puppet.puppet /etc/puppet
RUN chmod 755 /etc/puppet