<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @include('partials.header')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{!! (!empty($title)) ? $title: "Explore Magazine:Admin Panel" !!}</title>
</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">
    @include('partials.navbar-top')
    @include('partials.sidebar')
    <div class="content-wrapper">
        @yield('breadcumbs')
        @yield('content')
    </div>
    @include('partials.footer')
    @include('exploreAdmin.modal.error')

    <div class="control-sidebar-bg"></div>
</div>
@include('partials.scripts')

</body>
</html>