<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    @yield('title')

    @yield('css')
</head>
<body class="login-page">

@yield('content')

@yield('footer')

</body>
</html>