@extends('layouts.AppMain')
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
            font-family: 'Lato';
            font-weight: 100;
            color: #B0BEC5;
        }
        .box .alert {
            display: inline-block;

        }
    </style>
    @stop
@section('content')
    <section class="content">
    <div class="container">
        <div class="box box-primary">
            <div class="title">UnAuthorized Access..!</div>
            <div>
                <strong class="alert alert-danger">
                {{ $error }}
                </strong>
            </div>
        </div>
    </div>
    </section>
    @stop