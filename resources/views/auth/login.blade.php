@extends('layouts/appLogin')

@section('title')
    <title>Explore Magazine | Login</title>
    @stop
@section('css')
    <link rel="stylesheet" href="{!! url('vendor/login/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! url('vendor/login/css/AdminLTE.min.css') !!}">
    <link rel="stylesheet" href="{!! url('vendor/login/css/blue.css') !!}">
    <link rel="stylesheet" href="{!! url('vendor/login/css/fonts-awesome.min.css') !!}">
    {{--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />--}}
    @stop
@section('content')

    <div class="login-box">
        <div class="login-logo">
            <a href="{!! url() !!}" target="_blank"><b>Explore</b> Login</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            @if(Session::has('message'))
                <div class="callout callout-danger">
                    <h4>Invalid Login</h4>
                    <p>{{Session::get('message')}}</p>
                </div>
            @endif
            @if(Session::has('message_success'))
                <div class="callout callout-success">
                    <p>{{Session::get('message_success')}}</p>
                </div>
            @endif
            {!! Form::open(['url'=>'/auth/login']) !!}
            <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if($errors->has('email'))
                    {!! $errors->first('email', '<label class="control-label" for="inputError">:message</label>') !!}
                @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if($errors->has('password'))
                        {!! $errors->first('password', '<label class="control-label" for="inputError">:message</label>') !!}
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div><!-- /.col -->
                </div>
            {!! Form::close() !!}


            <a href="#">I forgot my password</a><br>
            <div class="login-box-footer">
                <p><small>&copy; copyright 2015 | Explore Magazine </small></p>
            </div>

        </div><!-- /.login-box-body -->
    </div>
    @stop
@section('footer')
    <script src="{!! url('vendor/login/js/bootstrap.min.js')!!}"></script>
    <script src="{!! url('vendor/login/js/jQuery-2.1.4.min.js')!!}"></script>
    <script src="{!! url('vendor/login/js/icheck.min.js')!!}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @stop