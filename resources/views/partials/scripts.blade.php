<script src="{!! asset('vendor/plugins/jQuery/jQuery-2.1.4.min.js') !!}" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

<script src="{!! asset('vendor/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
<script src="{!! asset('vendor/plugins/slimScroll/jquery.slimscroll.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('vendor/plugins/fastclick/fastclick.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('vendor/dist/js/app.min.js') !!}" type="text/javascript"></script>

<script src="{!! asset('vendor/dist/js/demo.js') !!}" type="text/javascript"></script>
<script src="{!! asset('vendor/dist/js/laravel.js') !!}" type="text/javascript"></script>
<script>
    $(document).ready(function(){
@if(Session::has('script')) :
{{ $script }}
    @endif
    });
</script>
@yield('scripts')