<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{!! asset('dist/img/user2-160x160.jpg') !!}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                {{--<p>Alexander Pierce</p>--}}
                <p>
                    {!! \Illuminate\Support\Facades\Auth::user()->first_name.' '.\Illuminate\Support\Facades\Auth::user()->last_name !!}
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            {{-- Main Menu/Links --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Main Menu/Links</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/menu') !!}"><i class="fa fa-circle-o"></i>View Menu</a></li>
                    <li><a href="{!! url('/menu/create') !!}"><i class="fa fa-circle-o"></i>Add Menu</a></li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Links <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{!! url('/links') !!}"><i class="fa fa-circle-o"></i>View Links</a></li>
                            <li><a href="{!! url('/links/create') !!}"><i class="fa fa-circle-o"></i>Add Links</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Nodes --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Nodes</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/nodes') !!}"><i class="fa fa-circle-o"></i>View Nodes</a></li>
                    <li><a href="{!! url('/nodes/create') !!}"><i class="fa fa-circle-o"></i>Add Node</a></li>
                    </ul>
            </li>
            {{-- Categories --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Categories</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/categories') !!}"><i class="fa fa-circle-o"></i>View Category</a></li>
                    <li><a href="{!! url('/categories/create') !!}"><i class="fa fa-circle-o"></i>Add Category</a></li>
                </ul>
            </li>
            {{--Articles--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Articles</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/articles') !!}"><i class="fa fa-circle-o"></i>View Articles</a></li>
                    <li><a href="{!! url('/articles/create') !!}"><i class="fa fa-circle-o"></i>Add Articles</a></li>
                </ul>
            </li>
            {{-- Locations --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Locations</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/locations') !!}"><i class="fa fa-circle-o"></i>View Locations</a></li>
                    <li><a href="{!! url('/locations/create') !!}"><i class="fa fa-circle-o"></i>Add Location</a></li>
                </ul>
            </li>
            {{-- Users --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars"></i> <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/users') !!}"><i class="fa fa-circle-o"></i>View Users</a></li>
                    <li><a href="{!! url('/users/create') !!}"><i class="fa fa-circle-o"></i>Add User</a></li>
                </ul>
            </li>
            {{-- Calendar --}}
            <li>
                <a href="pages/calendar.html">
                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                    <small class="label pull-right bg-red">3</small>
                </a>
            </li>
            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <small class="label pull-right bg-yellow">12</small>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>