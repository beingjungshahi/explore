<link href="{!! asset('vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('vendor/login/css/fonts-awesome.min.css') !!}" rel="stylesheet" type="text/css">
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"><link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />--}}
<link href="{!! asset('vendor/dist/css/AdminLTE.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('vendor/dist/css/skins/_all-skins.min.css') !!}" rel="stylesheet" type="text/css">

@yield('styles')


