@extends('layouts.AppMain')
@section('styles')

    <link href="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('vendor/dist/css/skins/_all-skins.min.css') !!}" rel="stylesheet" type="text/css"/>

@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Explore
            <small>List Nodes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url() !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Links</a></li>
        </ol>
    </section>
@stop
@section('content')
    <section class="content">
        <div class="row">
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                {!! session('message') !!}.
            </div>
            @endif
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Node List Data Table</h3>
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>NODE TITLE</th>
                                <th>NODE SLUG</th>
                                <th>NODE DESCRIPTION</th>
                                <th>STATUS</th>
                                <th>CREATED AT</th>
                                <th>MODIFIED AT</th>
                                <th>PERMISSION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $data as $node)
                                <tr>
                                    <td>{!! $node->title !!}</td>
                                    <td>{!! $node->slug !!}</td>
                                    <td>{!! $node->body !!}</td>
                                    <td>{!! ($node->published==1)?"<strong style = color:green>VISIBLE</strong>":'<strong style = color:red>DISABLE</strong>' !!}</td>
                                    <td>{!! $node->created !!}</td>
                                    <td>{!! $node->modified !!}</td>
                                    <td>
                                        {{--<a href=""><i class="fa fa-info-circle text-success" title="View"></i></a>--}}
                                        <a href=""><i class="fa fa-eye text-success" title="Add Links"></i></a>
                                        <a href="{!! route('nodes.edit',$node->id) !!}"><i
                                                    class="fa fa-pencil-square text-success" title="Edit"></i></a>
                                        <a href="{!! route('nodes.delete',$node->id) !!}"><i
                                                    class="fa fa-minus-circle text-danger" title="Delete"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>NODE TITLE</th>
                                <th>NODE SLUG</th>
                                <th>NODE DESCRIPTION</th>
                                <th>STATUS</th>
                                <th>CREATED AT</th>
                                <th>MODIFIED AT</th>
                                <th>PERMISSION</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/datatables/jquery.dataTables.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.min.js') !!}"
            type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $("#example2").DataTable();
//            $('#example2').DataTable({
//                "paging": true,
//                "lengthChange": false,
//                "searching": false,
//                "ordering": true,
//                "info": true,
//                "autoWidth": false
//            });
        });
    </script>
@stop