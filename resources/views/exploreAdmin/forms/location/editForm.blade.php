@extends('layouts.AppMain')

@section('styles')
    <link href="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}" rel="stylesheet"
          type="text/css"/>
@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Menu Links Attributes
            <small>Add Form</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{!! url('/locations') !!}">Locations</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($locations,['method'=>'PATCH','route'=>['location.edit',$locations->id],'role'=>'form','files'=>true,]) !!}
                    <div class="box-body">
                        <div>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    <li>{!! $error !!}</li>
                                @endforeach
                            @endif
                            @if(Session::has('message'))
                                <li>{{ session('message') }}</li>
                                @endif
                        </div>
                        <div class="form-group @if($errors->has('category_id')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">Category:</label>
                            {!! Form::select('category_id',$category,null,['class'=>'form-control']) !!}
                            @if($errors->has('category_id'))
                                {!! $errors->first('category_id', '<label class="control-label"
                                                                             for="inputError">Parent Menu Cannot be Blank.</label>') !!}
                            @endif
                        </div>

                        <div class="form-group @if($errors->has('name')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Name:</label>
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                            @if($errors->has('name'))
                                {!! $errors->first('name', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>

                        <div class="form-group @if($errors->has('slug')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Slug:</label>
                            {!! Form::text('slug', null, ['class'=>'form-control']) !!}                            @if($errors->has('slug'))
                                {!! $errors->first('slug', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('country')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Country:</label>
                            {!! Form::text('country', null, ['class'=>'form-control']) !!}                            @if($errors->has('country'))
                                {!! $errors->first('country', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('information')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Information:</label>
                            {!! Form::textarea('information',null,['class'=>'form-control','id'=>'editor1','rows'=>10,'cols'=>80,'placeholder'=>'Information']) !!}
                            @if($errors->has('information'))
                                {!! $errors->first('information', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                            <div class="form-group">
                                {!! Html::image('public/uploads/'.$image, $image, ['height'=>'100']) !!}
                                {{--<img src="{!! public_path('uploads/'.$file) !!}"/>--}}
                            </div>
                        <div class="form-group @if($errors->has('image')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Image:</label>
                            {!! Form::file('image',['class'=>'form-control']) !!}
                            @if($errors->has('image'))
                                {!! $errors->first('image', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>

                        <div class="bootstrap-datepicker">
                            <div class="form-group @if($errors->has('modified')) {{'has-error'}} @endif">
                                <label>Modify At:</label>

                                <div class="input-group">
                                    {!! Form::input('date','modified',\Carbon\Carbon::now()->format('Y-m-d'),['class' => 'form-control datepicker']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                                @if($errors->has('modified'))
                                    {!! $errors->first('modified', '<label class="control-label"
                                                                                 for="inputError">:message</label>') !!}
                                @endif
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                    </form>
                </div>
                <!-- /.box -->

                <!-- Form Element sizes -->

                {!! Form::close() !!}
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"
            type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        })
    </script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
@stop