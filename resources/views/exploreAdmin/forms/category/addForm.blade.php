@extends('layouts.AppMain')

@section('styles')
    <link href="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}" rel="stylesheet"
          type="text/css"/>
@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Add Category Attributes
            <small>Add Form</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{!! url('/categories') !!}">Category</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url'=>'/categories/create','role'=>'form']) !!}
                    <div class="box-body">
                        <div class="form-group @if($errors->has('parent_id')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">Parent:</label>
                            {!! Form::select('parent_id',$cat,null,['class'=>'form-control']) !!}
                            @if($errors->has('parent_id'))
                                {!! $errors->first('parent_id', '<label class="control-label"
                                                                             for="inputError">Parent Menu Cannot be Blank.</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('name')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Title:</label>
                            {!! Form::input('text','name',null,['class'=>'form-control','placeholder'=>'Title']) !!}
                            <span>
                            @if($errors->has('name'))
                                {!! $errors->all('name', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                            </span>
                        </div>
                        <div class="form-group @if($errors->has('slug')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Class:</label>
                            <input type="text" class="form-control" id="class" name="slug" placeholder="Slug">
                            @if($errors->has('slug'))
                                {!! $errors->first('slug', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('position')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Link:</label>
                            <input type="text" class="form-control" id="link" name="position" placeholder="Position">
                            @if($errors->has('position'))
                                {!! $errors->first('position', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('lft')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Target:</label>
                            <input type="text" class="form-control" id="target" name="lft" placeholder="Left">
                            @if($errors->has('lft'))
                                {!! $errors->first('lft', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('rght')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Right:</label>
                            <input type="text" class="form-control" id="rght" name="rght" placeholder="Right">
                            @if($errors->has('rght'))
                                {!! $errors->first('rght', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="bootstrap-datepicker">
                            <div class="form-group @if($errors->has('created')) {{'has-error'}} @endif">
                                <label>Created At:</label>

                                <div class="input-group">
                                    {!! Form::input('date','created',\Carbon\Carbon::now()->format('Y-m-d'),['class' => 'form-control datepicker']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                                @if($errors->has('created'))
                                    {!! $errors->first('created', '<label class="control-label"
                                                                                 for="inputError">:message</label>') !!}
                                @endif
                            </div>
                            <!-- /.form group -->
                        </div>
                        <div class="form-group @if($errors->has('active')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Status:</label>
                            {!! Form::select('active', [
                         '1'=> 'Active',
                         '0'=>'Deactivate',
                         ],null,['class'=>'form-control']) !!}
                            @if($errors->has('active'))
                                {!! $errors->first('active', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div>
                <!-- /.box -->

                <!-- Form Element sizes -->

                {!! Form::close() !!}
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"
            type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        })
    </script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
@stop