@extends('layouts.AppMain')

@section('styles')
    <link href="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}" rel="stylesheet" type="text/css" />
@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Main Menu Attributes
            <small>Edit Form</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{!! url('/menu') !!}">Menu</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit <strong>{!! $menu->title !!}</strong></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($menu,['method'=>'PATCH','action'=>['ExploreAdmin\ExploreMenuController@edit',$menu->slug],'role'=>'form']) !!}
                    <div class="box-body">
                        <div class="form-group @if($errors->has('title')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">Title:</label>
                            {!! Form::text('title', null,['class'=>'form-control']) !!}
                                <span>
                                    @if($errors->has('title'))
                                        {!! $errors->first('title', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('slug')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Slug:</label>
                            {!! Form::text('slug', null,['class'=>'form-control']) !!}
                                <span>
                                    @if($errors->has('slug'))
                                        {!! $errors->first('slug', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('class')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Class:</label>
                            {!! Form::text('class', null,['class'=>'form-control']) !!}
                                <span>
                                    @if($errors->has('class'))
                                        {!! $errors->first('class', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('description')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Description:</label>
                            {!! Form::textarea('description', null,['class'=>'form-control','id'=>'editor1','rows'=>10,'cols'=>80]) !!}
                            @if($errors->has('description'))
                                {!! $errors->first('description', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="bootstrap-datepicker">
                            <div class="form-group @if($errors->has('modified')) {{'has-error'}} @endif">
                                <label>Created At:</label>
                                <div class="input-group">
                                    {!! Form::input('date','modified',\Carbon\Carbon::now()->format('Y/m/d'),['class' => 'form-control datepicker']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div><!-- /.input group -->
                                    <span>
                                    @if($errors->has('modified'))
                                            {!! $errors->first('modified', '<label class="control-label"
                                                                                         for="inputError">:message</label>') !!}
                                        @endif
                                </span>
                            </div><!-- /.form group -->
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status:</label>
                            {!! Form::select('active', [
                         '1'=> 'Active',
                         '0'=>'Deactivate',
                         ],null,['class'=>'form-control']) !!}
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    </form>
                </div><!-- /.box -->

                <!-- Form Element sizes -->

                {!! Form::close() !!}
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        })
    </script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
@stop