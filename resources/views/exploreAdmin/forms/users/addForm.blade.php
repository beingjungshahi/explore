@extends('layouts.AppMain')


@section('breadcumbs')
    <section class="content-header">
        <h1>
            Explore
            <small>Add Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url() !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
    @stop

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Form</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url'=>'/users/create','role'=>'form']) !!}
                    <div class="box-body">
                        <div class="form-group @if($errors->has('first_name')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">First Name:</label>
                            {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>'Enter your first name']) !!}
                                <span>
                                    @if($errors->has('first_name'))
                                        {!! $errors->first('first_name', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('last_name')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Last Name:</label>
                            {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Enter your last name']) !!}
                                <span>
                                    @if($errors->has('last_name'))
                                        {!! $errors->first('last_name', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('username')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Username:</label>
                            {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Enter your username']) !!}
                                <span>
                                    @if($errors->has('username'))
                                        {!! $errors->first('username', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('email')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Email:</label>
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter your E-mail']) !!}
                                <span>
                                    @if($errors->has('email'))
                                        {!! $errors->first('email', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('password')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Password:</label>
                            {!! Form::password('password'   ,['class'=>'form-control','placeholder'=>'Enter your password']) !!}
                                <span>
                                    @if($errors->has('password'))
                                        {!! $errors->first('password', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('rPassword')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Retype Password:</label>
                            {!! Form::password('rPassword',['class'=>'form-control','placeholder'=>'Enter your Password Again']) !!}
                                <span>
                                    @if($errors->has('rPassword'))
                                        {!! $errors->first('rPassword', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('phone')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Phone:</label>
                            {!! Form::text('phone',null,['class'=>'form-control','placeholder'=>'Enter your Phone number']) !!}
                                <span>
                                    @if($errors->has('phone'))
                                        {!! $errors->first('phone', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('address')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Address:</label>
                            {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Enter your Address']) !!}
                                <span>
                                    @if($errors->has('address'))
                                        {!! $errors->first('address', '<label class="control-label"
                                                                                     for="inputError">:message</label>') !!}
                                    @endif
                                </span>
                        </div>
                        <div class="form-group @if($errors->has('country_id')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Country:</label>
                            {!! Form::select('country_id', [
                         '1'=> 'Active',
                         '0'=>'Deactivate',
                         ],null,['class'=>'form-control']) !!}
                            @if($errors->has('country_id'))
                                {!! $errors->first('country_id', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="bootstrap-datepicker">
                            <div class="form-group @if($errors->has('created')) {{'has-error'}} @endif">
                                <label>Created At:</label>
                                <div class="input-group">
                                    {!! Form::input('date','created',\Carbon\Carbon::now()->format('Y/m/d'),['class' => 'form-control datepicker']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div><!-- /.input group -->
                                    <span>
                                    @if($errors->has('created'))
                                            {!! $errors->first('created', '<label class="control-label"
                                                                                         for="inputError">:message</label>') !!}
                                        @endif
                                </span>
                            </div><!-- /.form group -->
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status:</label>
                            {!! Form::select('active', [
                         '1'=> 'Active',
                         '0'=>'Deactivate',
                         ],null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Role:</label>
                            {!! Form::select('role_id', [
                         '1'=> 'Admin',
                         '0'=>'User',
                         ],null,['class'=>'form-control']) !!}
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div><!-- /.box -->

                <!-- Form Element sizes -->

                {!! Form::close() !!}
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section>
    @stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        })
    </script>
    @stop