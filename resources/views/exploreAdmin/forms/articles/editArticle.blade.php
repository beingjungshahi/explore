@extends('layouts.AppMain')

@section('styles')
    <link href="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <style>
        .radio-label {
            display: inline-block;
            margin-left: 5px;
            margin-right: 10px;
        }
        .radio-input {
            display: inline-block;
            margin-left: 10px!important;
            margin-right: 5px!important;
        }
    </style>

@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Articles Attributes
            <small>Update Form</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{!! url('/links') !!}">Article</a></li>
            <li class="active">Update</li>
        </ol>
    </section>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($articles,['method'=>'PATCH','route'=>['articles.edit',$articles->id],'role'=>'form']) !!}
                    <div class="box-body">
                        <div class="form-group @if($errors->has('title')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Title:</label>
                            {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter Title']) !!}
                            @if($errors->has('title'))
                                {!! $errors->first('title', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('slug')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Slug:</label>
                            {!! Form::text('slug',null ,['class' => 'form-control', 'placeholder' => 'Enter Slug']) !!}
                            @if($errors->has('slug'))
                                {!! $errors->first('slug', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('show_home')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Show on home page:</label>
                            <label class="control-label radio-label"
                                   for="radioButton">{!! Form::radio('show_home','1' , true) !!}Yes</label>
                            <label class="control-label radio-label"
                                   for="radioButton">{!! Form::radio('show_home','0') !!}No</label>
                            @if($errors->has('show_home'))
                                {!! $errors->first('show_home', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('magazine_id')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">Magazine</label>
                            {!! Form::select('magazine_id',$magazine,null,['class'=>'form-control'])
                            !!}
                            @if($errors->has('magazine_id'))
                                {!! $errors->first('magazine_id', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group @if($errors->has('category_id')) {{'has-error'}} @endif">
                            <label for="exampleInputEmail1">Category</label>
                            {!! Form::select('category_id',$category,null,['class'=>'form-control'])
                            !!}
                            @if($errors->has('category_id'))
                                {!! $errors->first('category_id', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Location</label>
                            {!! Form::select('location_id',$location,null,['class'=>'form-control'])
                            !!}
                        </div>
                        <div class="form-group @if($errors->has('content')) {{'has-error'}} @endif">
                            <label for="exampleInputPassword1">Content:</label>
                            {!! Form::textarea('content',null ,['class' => 'form-control','id '=>'editor1','row'=>'10','cols'=>'80' ,'placeholder' => 'Enter Content']) !!}
                            @if($errors->has('content'))
                                {!! $errors->first('content', '<label class="control-label"
                                                                             for="inputError">:message</label>') !!}
                            @endif
                        </div>
                        <div class="bootstrap-datepicker">
                            <div class="form-group">
                                <label>Created At:</label>

                                <div class="input-group">
                                    {!! Form::input('date','modified',\Carbon\Carbon::now()->format('Y-m-d'),['class' => 'form-control datepicker']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div>
                                @if($edited)
                                @foreach($edited as $edit)
                                {!! $edit !!}
                                @endforeach
                                @endif
                                        <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputPassword1">Status:</label>--}}
                        {{--{!! Form::select('edited_type', [--}}
                        {{--'1'=> 'Active',--}}
                        {{--'0'=>'Deactivate',--}}
                        {{--],null,['class'=>'form-control']) !!}--}}
                        {{--</div>--}}

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div>
                <!-- /.box -->

                <!-- Form Element sizes -->

                {!! Form::close() !!}
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"
            type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        })
    </script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
@stop