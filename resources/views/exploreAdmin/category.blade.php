@extends('layouts.AppMain')
@section('styles')

    <link href="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('vendor/dist/css/skins/_all-skins.min.css') !!}" rel="stylesheet" type="text/css"/>

@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Explore
            <small>List Menu</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url() !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Category</a></li>
        </ol>
    </section>
@stop
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Category List Data Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>TITLE</th>
                                <th>SLUG</th>
                                <th>CREATED</th>
                                <th>MODIFIED</th>
                                <th>STATUS</th>
                                <th>PERMISSION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $category as $cat)
                                <tr>
                                    <td>{!! $cat->name !!}</td>
                                    <td>{!! $cat->slug !!}</td>
                                    <td>{!! $cat->created !!}</td>
                                    <td>{!! $cat->modified !!}</td>
                                    <td>{!! ($cat->active==1)?"<strong style = color:green>VISIBLE</strong>":'<strong style = color:red>DISABLE</strong>' !!}</td>
                                    <td>
                                        {{--<a href=""><i class="fa fa-info-circle text-success" title="View"></i></a>--}}
                                        <a href=""><i class="fa fa-link text-success" title="Add Links"></i></a>
                                        <a href="{!! action('ExploreAdmin\ExploreCategoryController@edit',$cat->id) !!}"><i
                                                    class="fa fa-pencil-square text-success" title="Edit"></i></a>
                                        <a href="{!! route('category.delete',$cat->id) !!}"><i
                                                    class="fa fa-minus-circle text-danger" title="Delete"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>TITLE</th>
                                <th>SLUG</th>
                                <th>CREATED</th>
                                <th>MODIFIED</th>
                                <th>STATUS</th>
                                <th>PERMISSION</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/datatables/jquery.dataTables.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.min.js') !!}"
            type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $("#example2").DataTable();
//            $('#example2').DataTable({
//                "paging": true,
//                "lengthChange": false,
//                "searching": false,
//                "ordering": true,
//                "info": true,
//                "autoWidth": false
//            });
        });
    </script>
@stop