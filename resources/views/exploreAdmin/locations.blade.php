@extends('layouts.AppMain')
@section('styles')

    <link href="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.css') !!}" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('vendor/dist/css/skins/_all-skins.min.css') !!}" rel="stylesheet" type="text/css"/>

@stop
@section('breadcumbs')
    <section class="content-header">
        <h1>
            Explore
            <small>List Menu</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url() !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Locations</a></li>
        </ol>
    </section>
@stop
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Location List Data Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>LOCATION TITLE</th>
                                <th>CATEGORY NAME</th>
                                <th>SLUG</th>
                                <th>COUNTRY</th>
                                <th>INFORMATION</th>
                                <th>IMAGE</th>
                                <th>CREATED AT</th>
                                <th>MODIFIED AT</th>
                                <th>PERMISSION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $locations as $location)
                                <tr>
                                    <td>{!! $location->name !!}</td>
                                    <td>{!! $location->category->name !!}</td>
                                    <td>{!! $location->slug !!}</td>
                                    <td>{!! $location->country !!}</td>
                                    <td>{!! $location->information !!}</td>
                                    <td>{!! Html::image('public/uploads/'.$location->image, $location->image, ['height'=>'80']) !!}</td>
                                    <td>{!! $location->created !!}</td>
                                    <td>{!! $location->modified !!}</td>
                                    <td>
                                        {{--<a href=""><i class="fa fa-info-circle text-success" title="View"></i></a>--}}
                                        <a href=""><i class="fa fa-eye text-success" title="View Location"></i></a>

                                        <a href="{!! action('ExploreAdmin\ExploreLocationController@edit',$location->id) !!}"><i
                                                    class="fa fa-pencil-square text-success" title="Edit"></i>
                                        </a>

                                        {{--<a href="{!! route('location.delete',$location->id) !!}"><i--}}
                                                    {{--class="fa fa-minus-circle text-danger" title="Delete"></i></a>--}}

                                        <a href="{!! route('location.delete',$location->id) !!}" data-method="delete"
                                           data-token="{{csrf_token()}}" data-confirm="Are you sure?">
                                            <i class="fa fa-minus-circle text-danger" title="delete"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>LOCATION TITLE</th>
                                <th>CATEGORY NAME</th>
                                <th>SLUG</th>
                                <th>COUNTRY</th>
                                <th>INFORMATION</th>
                                <th>IMAGE</th>
                                <th>CREATED AT</th>
                                <th>MODIFIED AT</th>
                                <th>PERMISSION</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{!! asset('vendor/plugins/datatables/jquery.dataTables.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('vendor/plugins/datatables/dataTables.bootstrap.min.js') !!}"
            type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $("#example2").DataTable();
//            $('#example2').DataTable({
//                "paging": true,
//                "lengthChange": false,
//                "searching": false,
//                "ordering": true,
//                "info": true,
//                "autoWidth": false
//            });
        });
    </script>
@stop