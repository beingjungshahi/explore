<?php
/**
 * Created by PhpStorm.
 * User: adittyajungshahi
 * Date: 11/6/15
 * Time: 8:17 AM
 */
namespace App\Validation;


class CustomValidator extends \Illuminate\Validation\Validator
{
	public function validateAlphaSpace($attribute, $value, $params) {

		return preg_match('/^[\pL\s]+$/u', $value);
	}
}