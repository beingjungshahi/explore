<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/**
 * Frontend Routes
 */
Route::get('/', function () {
	return view('welcome');
});
/**
 * Backend Routes
 */
Route::get('admin', function () {
	return view('auth/login');
});
/**
 * Routing Auth Controller
 */
Route::controllers(
	[
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController'
	]
);
/**
 * Model Bindings
 */
Route::bind('menu', function ($slug) {
	return App\Models\Menu::where('slug', $slug)->first();
});
Route::bind('links', function ($id) {
	return App\Models\Links::where('id', $id)->first();
});

//	Files
Route::get('public/uploads/{file}', function($file) {
	return response(file_get_contents(public_path().'/uploads/'.$file), 202)->header('Content-Type', 'image/jpeg');
});
/**
 * Securing Routes using middleware
 */
Route::group(['middleware' => 'auth'], function () {

//    Menu Routes

	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'ExploreAdmin\ExploreDashboardController@index']);
	Route::get('menu', 'ExploreAdmin\ExploreMenuController@index');
	Route::get('menu/create', 'ExploreAdmin\ExploreMenuController@create');
	Route::post('menu/create', 'ExploreAdmin\ExploreMenuController@store');
	Route::get('menu/{menu}', 'ExploreAdmin\ExploreMenuController@show');
	Route::get('menu/{menu}/edit', 'ExploreAdmin\ExploreMenuController@edit');
	Route::patch('menu/{menu}/edit', 'ExploreAdmin\ExploreMenuController@update');
	Route::get('menu/{menu}/delete', ['as' => 'menu.delete', 'uses' => 'ExploreAdmin\ExploreMenuController@destroy']);

//    Link Routes

	Route::get('links', 'ExploreAdmin\ExploreLinksController@index');
	Route::get('links/create', 'ExploreAdmin\ExploreLinksController@create');
	Route::post('links/create', 'ExploreAdmin\ExploreLinksController@store');
	Route::get('links/{links}', 'ExploreAdmin\ExploreLinksController@show');
	Route::get('links/{links}/edit', 'ExploreAdmin\ExploreLinksController@edit');
	Route::patch('links/{links}/edit', 'ExploreAdmin\ExploreLinksController@update');
	Route::get('links/{links}/delete', ['as' => 'links.delete', 'uses' => 'ExploreAdmin\ExploreLinksController@destroy']);

//    Node Routes
	Route::get('nodes', 'ExploreAdmin\ExploreNodesController@index');
	Route::get('nodes/create', 'ExploreAdmin\ExploreNodesController@create');
	Route::post('nodes/create', ['as' => 'nodes.create', 'uses' => 'ExploreAdmin\ExploreNodesController@store']);
	Route::get('nodes/{nodes}', 'ExploreAdmin\ExploreNodesController@show');
	Route::get('nodes/{nodes}/edit', ['as' => 'nodes.edit', 'uses' => 'ExploreAdmin\ExploreNodesController@edit']);
	Route::patch('nodes/{nodes}/edit', ['as' => 'nodes.edit', 'uses' => 'ExploreAdmin\ExploreNodesController@update']);
	Route::get('nodes/{nodes}/delete', ['as' => 'nodes.delete', 'uses' => 'ExploreAdmin\ExploreNodesController@destroy']);

//    User Routes
	Route::get('users', 'ExploreAdmin\ExploreUserController@index');
	Route::get('users/create', 'ExploreAdmin\ExploreUserController@create');
	Route::post('users/create', 'ExploreAdmin\ExploreUserController@store');

//    Category Routes
	Route::get('categories', 'ExploreAdmin\ExploreCategoryController@index');
	Route::get('categories/create', 'ExploreAdmin\ExploreCategoryController@create');
	Route::post('categories/create', 'ExploreAdmin\ExploreCategoryController@store');
	Route::get('categories/{id}','ExploreAdmin\ExploreCategoryController@show');
	Route::get('categories/{id}/edit', ['as' => 'categories.edit', 'uses' => 'ExploreAdmin\ExploreCategoryController@edit']);
	Route::patch('categories/{id}/update', 'ExploreAdmin\ExploreCategoryController@update');
	Route::delete('categories/{id}/delete', ['as' => 'category.delete', 'uses' => 'ExploreAdmin\ExploreCategoryController@destroy']);

//    Location Routes
	Route::get('locations', 'ExploreAdmin\ExploreLocationController@index');
	Route::get('locations/create', 'ExploreAdmin\ExploreLocationController@create');
	Route::post('locations/create', 'ExploreAdmin\ExploreLocationController@store');
	Route::get('locations/{id}/edit', 'ExploreAdmin\ExploreLocationController@edit');
	Route::patch('locations/{id}/edit', ['as'=>'location.edit','uses'=>'ExploreAdmin\ExploreLocationController@update']);
	Route::delete('locations/{id}/delete', ['as' => 'location.delete', 'uses' => 'ExploreAdmin\ExploreLocationController@destroy']);


//	Articles Rotes
	Route::get('articles/','ExploreAdmin\ExploreArticlesController@index');
	Route::get('articles/create','ExploreAdmin\ExploreArticlesController@create');
	Route::post('articles/create','ExploreAdmin\ExploreArticlesController@store');
	Route::get('articles/{id}',['as'=>'articles.show', 'uses'=>'ExploreAdmin\ExploreArticlesController@show']);
	Route::get('articles/{id}/edit', 'ExploreAdmin\ExploreArticlesController@edit');
	Route::patch('articles/{id}/edit',['as'=>'articles.edit','uses'=>'ExploreAdmin\ExploreArticlesController@update']);
	Route::delete('articles/{id}/delete', ['as'=>'articles.delete','uses'=>'ExploreAdmin\ExploreArticlesController@destroy']);


});


/**
 * Print Executed SQL Queries
 */
//Event::listen('illuminate.query', function($query)
//{
//    dd($query);
//});
