<?php

namespace App\Http\Controllers\ExploreAdmin;

use App\Models\Category;
use App\Models\Locations;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\Http\Requests\LocationsRequest;
use Illuminate\Support\Facades\File;
use Validator;
use Session;

class ExploreLocationController extends BaseController
{
    protected $filesystem;
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Locations::with('category')->orderBy('id','desc')->paginate(10);
        return view('exploreAdmin.locations',compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Locations $locations)
    {
        $category = Category::lists('name','id');
        $category[] = 'None';
        return view('exploreAdmin.Forms.location.addForm',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationsRequest $request)
    {
        $file = $request->file('image');
        $imageName = Carbon::now()->toDateTimeString().'_'.$file->getClientOriginalName();
        $destinationPath = public_path() . '/uploads';
        $validation = Validator::make($request->all(), ['image'=>'required|image']);
        if($validation->fails()) {
            return redirect()->back()->withInput()->with('message', $validation->errors());
        }
        $data = [
            'category_id' => $request->input('category_id'),
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'country' => $request->input('country'),
            'information' => $request->input('information'),
            'image' => $imageName,
            'created'=> $request->input('created')
        ];

        if ($file->move($destinationPath, $imageName)) {
                $locations = new Locations($data);
                $locations->save();
                return redirect('/locations');
        }

        return redirect('locations/create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Locations $locations, $id)
    {
        $locations = $locations->findOrFail($id);
        $category = Category::lists('name','id');
        $category[] = 'None';
        return view('exploreAdmin.forms.location.editForm', compact('locations'))->with([
            'category'=>$category,
            'image'=>$locations->image,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LocationsRequest $request, Locations $locations, $id)
    {
        $locations = $locations->findOrFail($id);
        if($request->hasFile('image')) :
            $file = $request->file('image');
            $validation = Validator::make($request->all(), [
            'image'=> 'image'
        ]);
        if($validation->fails()) {
            Session::flash('message',$validation->errors());
            return redirect()->back();
        }
            $imageName = Carbon::now()->toDateTimeString() . '_' . $file->getClientOriginalName();
            if($this->filesystem->exists($locations->image)) :
                $this->filesystem->delete($locations->image);
                $this->filesystem->put($imageName, File::get($file));
            endif;
            $data = [
                'category_id'=>$request->get('category_id'),
                'slug'       =>$request->get('slug'),
                'name'       =>$request->get('name'),
                'country'    =>$request->get('country'),
                'information'=>$request->get('information'),
                'image'      =>$imageName,
                'modified'   =>$request->get('modified')
            ];

            $locations->fill($data);
            $locations->update();
        else :
            $locations->update($request->except('image'));
        endif;
        return redirect('locations');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Locations $locations, $id)
    {
        $locations->destroy($id);
        return redirect('locations')->with('message','Record deleted successfully');
    }
}
