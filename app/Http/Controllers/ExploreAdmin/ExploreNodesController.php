<?php

namespace App\Http\Controllers\ExploreAdmin;

use App\Models\Nodes;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Session;
use Carbon\Carbon;

class ExploreNodesController extends BaseController
{
    protected $rules;

    function __construct() {
        parent::__construct();
         $this->rules = [
            'title' => 'required',
            'slug' => 'required',
            'body' => 'required|min:5',
            'published' => 'required|boolean',
            'created',
            'modified'
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Nodes $nodes)
    {
        $data = $nodes->orderBy('id','desc')->paginate(10);
        return view('exploreAdmin.nodes', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exploreAdmin.forms.nodes.addNodes');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Nodes $nodes)
    {
        $this->validate($request, $this->rules);
        if($nodes->create($request->all())) :
        return redirect('nodes')->with('message','Record created Successfully.');
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Nodes $nodes, $id)
    {
//        $nodes = Nodes::findOrFail($id);
        $nodes = $nodes->findOrFail($id);
        return view('exploreAdmin.forms.nodes.editNode',compact('nodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nodes $nodes, $id)
    {
        $nodes = $nodes->findOrFail($id);
        $this->validate($request,$this->rules);
//        dd($request->except('_method','_token'));
        if($nodes->update($request->all())) :
        return redirect('nodes')->with('message','Record Saved Successfully');
        else :
            return redirect('nodes/{nodes}/edit')->with('message','Record Cannot be Updated');
            endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nodes $nodes,$id)
    {
        $nodes->destroy($id);
        return redirect('nodes');
    }
}
