<?php

namespace App\Http\Controllers\ExploreAdmin;

use App\Models\Menu;
use App\Models\Nodes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Links;
use Carbon\Carbon;
class ExploreLinksController extends Controller
{

    protected $rules = [
        'parent_id'=>'required',
        'menu_id'=>'required',
        'node_id'=>'required',
        'title'=>'required|min:3',
        'class',
        'description'=>'required|min:5',
        'link'=>'required',
        'target',
        'rel',
        'active'=>'required|boolean',
        'lft',
        'rght',
        'modified',
        'created',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Links $links)
    {
//        $test = $links->with('menu.submenu', 'nodes')->get(); dd($test->toArray());
//        $menus = $links
//            ->join('menus','menus.id','=','links.menu_id')
//            ->join('nodes','nodes.id','=','links.node_id')
//            ->select(
//                '*','links.title as ltitle',
//                'nodes.title as node_title',
//                'menus.title as menu_title'
//            )
//            ->paginate(10);
        $menus = $links->with('menu', 'nodes','links')->paginate(10);
//        dd($menus->toArray());
        return view('exploreAdmin.links',['links'=>$menus])->with('title','Links::Explore Magazine|Admin Panel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Links $links)
    {
//        $menus = $links
//            ->join('menus','links.menu_id','=','menus.id')
//            ->join('nodes','links.node_id','=','nodes.id')
//            ->select(
//                '*',
//                'links.title as link_title',
//                'nodes.title as node_title',
//                'menus.title as menu_title'
//            )
//            ->get();
//            dd($menus->toArray());
        $menus = $links->with('menu','nodes')->get();
        $parent = $links->lists('title','id');
        $parent[] = 'None';

        $menu = Menu::lists('title','id');
        $menu[] = 'None';

        $node = Nodes::lists('title','id');
        $node[] = 'None';
//        dd($menu);
        return view('exploreAdmin.forms.links.linksAdd',
            [
                'links' => $menus,
                'parent' => $parent,
                'menu' => $menu,
                'node' => $node
            ]
        )->with('title','ADD::Links|Explore Magazine');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        Links::create($request->all());
        return redirect('links');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Links $links)
    {
        $parent = $links->lists('title','id');
        $parent[] = 'None';

        $menu = Menu::lists('title','id');
        $menu[] = 'None';

        $node = Nodes::lists('title','id');
        $node[] = 'None';
//        dd($menu);
        return view('exploreAdmin.forms.links.linksEdit',compact('links'))->with([
            'title'=>'Edit::'.$links->title,
            'parent' => $parent,
            'menu' => $menu,
            'node' => $node
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Links $links)
    {
        $this->validate($request, $this->rules);
        $links->update($request->all());
        return redirect('links');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Links $links)
    {
        $links->delete();
        return redirect('links');
    }
}
