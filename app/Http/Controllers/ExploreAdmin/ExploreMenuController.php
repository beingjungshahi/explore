<?php

namespace App\Http\Controllers\ExploreAdmin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\MenuRequest;
use App\Models\Menu;

class ExploreMenuController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::paginate(0);
        return view('exploreAdmin.menu',['menu'=> $menu])->with('title','Menu::Explore Magazine|Admin Panel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exploreAdmin.forms.menu.addForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $menuRequest)
    {
        $data = $menuRequest->all();
        Menu::create($data);
        return redirect('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
//        $menu = $this->menu->where('slug',$slug)->first();
//        $menu = Menu::findOrFail($slug);
        return view('exploreAdmin.forms.menu.editForm',compact('menu'))->with([
                'title'=>'Edit::'.$menu->title,
                'class'=>'active',
        ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $menuRequest, Menu $menu)
    {
        $menu->update($menuRequest->all());
        return redirect('menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect('menu')->with('message','Record Deleted Successfully !');
    }

}
