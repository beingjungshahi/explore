<?php

namespace App\Http\Controllers\ExploreAdmin;

use App\Models\Users;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ExploreUserController extends BaseController
{

    public $rules = [
        'first_name'=> 'required|alpha_space|string',
        'last_name' => 'required|alpha',
        'username' => 'required|alpha_dash|alpha_num|unique:users,username',
        'email' =>'required|email|unique:users,email',
        'password' => 'required',
        'rPassword' =>'required|same:password',
        'phone' => 'required',
        'address' => 'required',
        'country_id' => 'required',
        'created' => 'required',
        'active' =>'required',
        'role_id' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        dd($this->access());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = $this->getRole();
        if($this->access() == '777') :
        return view('exploreAdmin.forms.users.addForm');
            else :
                return view('errors.403')->with('error',"You Don't have sufficient permission to access this Link" );
                endif;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'rPassword.required' => 'The Re-Type Password field is required.',
            'rPassword.same' => ' The Re-Type password and password must match. ',
        ];
        $this->validate($request,$this->rules,$messages);
        Users::create($request->all());
        return redirect('users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
