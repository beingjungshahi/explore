<?php
/**
 * Created by PhpStorm.
 * User: adittyajungshahi
 * Date: 10/1/15
 * Time: 12:10 PM
 */

namespace App\Http\Controllers\ExploreAdmin;

use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;

class BaseController extends Controller
{
	function __construct()
	{
		// share current route in all views
		$currentRoute = Route::getCurrentRoute();
		if($currentRoute) :
		View::share('current_url', $currentRoute->getPath());
			endif;
		// share current logged in user details all views
		View::share('current_user', $this->currentUser());
	}

	public function currentUser()
	{
		$current_user = Auth::user();
		return $current_user;
	}
	public function getRole() {
		$user = new Users();
		$roles = $user->with('roles.permissions')->findOrFail($this->currentUser()->id);
		return $roles;
	}
	public function access() {
		if($this->getRole($this->currentUser()->id)->roles->permissions->name == 'ROOT') :
			return $this->getRole($this->currentUser()->id)->roles->permissions->value;
			elseif($this->getRole($this->currentUser()->id)->roles->permissions->name == 'READ/WRITE') :
				return $this->getRole($this->currentUser()->id)->roles->permissions->value;
				else :
					return '555';
					endif;
	}
}