<?php

namespace App\Http\Controllers\ExploreAdmin;

use App\Models\Links;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;

class ExploreCategoryController extends Controller
{
    protected $rules = [
        'parent_id'=>'required',
        'name'=>'required|alpha|min:3',
        'position',
        'lft',
        'rght',
        'active'=>'required|boolean',
        'modified',
        'created',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::orderBy('id','desc')->paginate(10);
        return view('exploreAdmin.category',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $cat = $category->lists('name','id');
        $cat[] = 'None';
        return view('exploreAdmin.forms.category.addForm', compact('cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
//        dd($request->all());
        Category::create($request->all());
        return redirect('categories')->with('success','Record Saved Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, $id)
    {
        $category = $category->findOrFail($id);
        $cat = $category->lists('name','id');
        return view('exploreAdmin.forms.category.editForm', compact('category'))->with(['cat'=>$cat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category, $id)
    {
        $category = $category->findOrFail($id);
        $this->validate($request, $this->rules);
        $category->update($request->all());
        return redirect('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
