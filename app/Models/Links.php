<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    public $timestamps = false;
	protected $table = 'links';
	protected $fillable = [
		'parent_id',
		'menu_id',
		'node_id',
		'title',
		'class',
		'description',
		'link',
		'target',
		'rel',
		'active',
		'lft',
		'rght',
		'modified',
		'created',

	];


	function menu()
	{
		return $this->belongsTo('App\Models\Menu', 'menu_id');
	}

	function nodes()
	{
		return $this->belongsTo('App\Models\Nodes', 'node_id');
	}
	function links()
	{
		return $this->hasOne('App\Models\Links','parent_id');
	}


}
