<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nodes extends Model
{
    public $timestamps = false;
	protected $table = 'nodes';
	protected $fillable = [
		'title',
		'slug',
		'body',
		'published',
		'created',
		'modified',
	];

}
