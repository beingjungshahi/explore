<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
	public $timestamps = false;
	protected $table = 'users';
	protected $fillable = [
		'first_name',
		'last_name',
		'username',
		'email',
		'password',
		'phone',
		'address',
		'country_id',
		'created',
		'active',
		'role_id'
	];

	public function roles() {
		return $this->hasOne('App\Models\Roles','id','role_id');
	}

}
