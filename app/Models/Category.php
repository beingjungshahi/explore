<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

	protected $table = 'categories';
	/**
	 * @var array
	 */
	protected $fillable = [
		'parent_id',
		'name',
		'position',
		'lft',
		'rght',
		'active',
		'modified',
		'created',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 * @return parent_id and in related data.
	 */


}
