<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    public $timestamps = false;

	protected $table = 'articles';
	protected $fillable = [];

	public function user() {
		return $this->belongsTo('App\User','user_id');
	}

	public function category() {
		return $this->belongsTo('App\Models\Category','category_id');
	}

	public function location() {
		return $this->belongsTo('App\Models\Locations','location_id');
	}

	public function magazine() {
		return $this->belongsTo('App\Models\Magazine','magazine_id');
	}


}
