<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    public $timestamps = false;

	protected $table = 'locations';
	protected $fillable = [
		'category_id',
		'name',
		'slug',
		'country',
		'information',
		'image',
		'created',
		'modified'

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function category() {
		return $this->belongsTo('App\Models\Category','category_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function location() {
		return $this->hasOne('App\Models\Locations','id');
	}
}
