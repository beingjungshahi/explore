<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public $timestamps = false;

	protected $table = 'roles';

	public function permissions()
	{
		return $this->hasOne('App\Models\Permissions','id','permission_id');
	}
}
